import React, { useState, useEffect } from 'react';

interface Todo {
    id: number;
    title: string;
}

const TodoList: React.FC = () => {
    const [todos, setTodos] = useState<Todo[]>([]);
    const [newTodo, setNewTodo] = useState<string>('');

    useEffect(() => {
        fetch('http://localhost:4000/todos')
            .then((response) => response.json())
            .then((data) => setTodos(data));
    }, []);

    const handleAddTodo = () => {
        fetch('http://localhost:4000/todos', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ title: newTodo }),
        })
            .then((response) => response.json())
            .then((todo) => {
                setTodos([...todos, todo]);
                setNewTodo('');
            });
    };

    const handleDeleteTodo = (id: number) => {
        fetch(`http://localhost:4000/todos/${id}`, {
            method: 'DELETE',
        })
            .then(() => {
                const updatedTodos = todos.filter((todo) => todo.id !== id);
                setTodos(updatedTodos);
            });
    };

    return (
        <div>
            <h1>Todo Liste</h1>
            <ul>
                {todos.map((todo) => (
                    <li key={todo.id}>
                        {todo.title}
                        <button onClick={() => handleDeleteTodo(todo.id)}>Löschen</button>
                    </li>
                ))}
            </ul>
            <div>
                <input
                    value={newTodo}
                    onChange={(e) => setNewTodo(e.target.value)}
                    placeholder="Neues Todo"
                />
                <button onClick={handleAddTodo}>Hinzufügen</button>
            </div>
        </div>
    );
}

export default TodoList;
