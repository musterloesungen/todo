# --- Build Stage ---
FROM node:16 AS build

# Set the working directory in the builder
WORKDIR /app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the source files
COPY . .

# Build the React app
RUN npm run build

# --- Run Stage ---
FROM nginx:alpine

# Copy built app to nginx serve directory
COPY --from=build /app/build /usr/share/nginx/html

# Expose port 80
EXPOSE 80

# Start nginx server
CMD ["nginx", "-g", "daemon off;"]
